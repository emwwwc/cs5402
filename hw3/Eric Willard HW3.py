
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
from sklearn.naive_bayes import GaussianNB
gnb = GaussianNB()

train_df = pd.read_csv("./train.csv")
train_df.head()


# In[4]:


train_data = train_df.drop("Label", axis=1)
train_label = train_df["Label"]
bayes = gnb.fit(train_data, train_label)

test_df = pd.read_csv("./test.csv")
test_data = test_df.drop("Label", axis=1)
y_pred = bayes.predict(test_data)
print(y_pred)


# In[5]:


expected = test_df["Label"].tolist()
tp = 0
fp = 0
tn = 0
fn = 0

for i in range(0, len(expected)):
    if y_pred[i] == 1 and expected[i] == 1:
        tp += 1
    if y_pred[i] == 1 and expected[i] == 0:
        fp += 1
    if y_pred[i] == 0 and expected[i] == 0:
        tn += 1
    if y_pred[i] == 0 and expected[i] == 1:
        fn += 1
    
recall = (tp/(tp + fn))
precision = (tp/(tp + fp))
        
print('Accuray:', ((tp + tn)/(tp + tn + fp + fn))*100, '%')
print('Precision:', precision*100, '%')
print('Recall:', recall*100, '%')
print('F1:', ((2*(recall*precision))/(recall+precision))*100, '%')


# ## ID3
# Accuray: 91.66666666666666 %
# Precision: 100.0 %
# Recall: 88.88888888888889 %
# F1: 94.11764705882352 %
# 
# ## C4.5
# Accuray: 75.0 %
# Precision: 100.0 %
# Recall: 66.66666666666666 %
# F1: 80.0 %
