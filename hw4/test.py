import pandas as pd
from sklearn import tree
from sklearn.neighbors import KNeighborsClassifier 
import pickle

def testTree(filename):
    df = pd.read_csv(filename)
    X = df.drop(df.columns[-1], axis=1)
    Y = df[df.columns[-1]]
    decision_tree_pkl_filename = 'train_tree.pkl'
    decision_tree_pkl_file = open(decision_tree_pkl_filename, 'rb')
    tree = pickle.load(decision_tree_pkl_file)
    return tree.predict(X), Y.values.tolist()

def testKNN(filename):
    df = pd.read_csv(filename)
    X = df.drop(df.columns[-1], axis=1)
    Y = df[df.columns[-1]]
    knn_pkl_filename = 'train_knn.pkl'
    knn_pkl_file = open(knn_pkl_filename, 'rb')
    neigh = pickle.load(knn_pkl_file)
    return neigh.predict(X), Y.values.tolist()