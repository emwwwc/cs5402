import pandas as pd
from sklearn import tree
from sklearn.neighbors import KNeighborsClassifier 
import pickle

def trainTree(filename):
    df = pd.read_csv(filename)
    X = df.drop(df.columns[-1], axis=1)
    Y = df[df.columns[-1]]
    clf = tree.DecisionTreeClassifier()
    clf.fit(X,Y)
    decision_tree_pkl_filename = 'train_tree.pkl'
    decision_tree_pkl_file = open(decision_tree_pkl_filename, 'wb')
    pickle.dump(clf, decision_tree_pkl_file)
    decision_tree_pkl_file.close()

def trainKNN(filename, k):
    df = pd.read_csv(filename)
    X = df.drop(df.columns[-1], axis=1)
    Y = df[df.columns[-1]]
    neigh = KNeighborsClassifier(n_neighbors = k)
    neigh.fit(X, Y)
    knn_pkl_filename = 'train_knn.pkl'
    knn_pkl_file = open(knn_pkl_filename, 'wb')
    pickle.dump(neigh, knn_pkl_file)
    knn_pkl_file.close()