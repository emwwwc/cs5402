from sklearn.metrics import classification_report
from collections import defaultdict
import pandas as pd
import numpy as np
import train
import test
import random
import matplotlib.pyplot as plt

train_partition_filename = 'train.csv'
test_partition_filename = 'test.csv'

def createPartitions():
    df = pd.read_csv('iris.csv')
    # randomly arrange dataframe rows
    rand_df = df.sample(frac = 1)
    # create training and testing csv files
    train_file = open(train_partition_filename, 'w')
    test_file = open(test_partition_filename, 'w')
    # every 5th row is put into a different file
    for i in range(len(rand_df)):
        if (i % 5 == 0):
            test_file.write(",".join(map(str, rand_df.loc[[i]].values.tolist()[0])))
            test_file.write("\n")
        else:
            train_file.write(",".join(map(str, rand_df.loc[[i]].values.tolist()[0])))
            train_file.write("\n")


def report2dict(cr):
    # Parse rows
    tmp = list()
    for row in cr.split("\n"):
        parsed_row = [x for x in row.split("  ") if len(x) > 0]
        if len(parsed_row) > 0:
            tmp.append(parsed_row)
    
    # Store in dictionary
    measures = tmp[0]

    D_class_data = defaultdict(dict)
    for row in tmp[1:]:
        class_label = row[0]
        for j, m in enumerate(measures):
            D_class_data[class_label][m.strip()] = float(row[j + 1].strip())
    return D_class_data

def calculatePerformance(predicted, expected):
    report = report2dict(classification_report(expected, predicted))
    correct = 0
    for i in range(0, len(expected)):
        if predicted[i] == expected[i]:
            correct += 1
    accuracy = correct / len(expected)

    precision = 0
    recall = 0
    f1score = 0
    for label in report.values():
        precision += label['precision']
        recall += label['recall']
        f1score += label['f1-score']

    return {
        'accuracy' : "%.2f" % accuracy,
        'precision': (precision/len(report)),
        'recall' : (recall/len(report)),
        'f1' : (f1score/len(report))
    }   

def graphKNN():
    accuracies = []
    f1s = []
    for i in range(1, 20):
        train.trainKNN(train_partition_filename, i)
        knn_predict, knn_expected = test.testKNN(test_partition_filename)
        performance = calculatePerformance(knn_predict, knn_expected)
        accuracies.append(performance['accuracy'])
        f1s.append(performance['f1'])
    
    bar_width = .35

    plt.bar([i for i in range(1, 20)], [float(i) for i in accuracies], bar_width, alpha=0.8, color='b', label='Accuracies')
    plt.bar([i + bar_width for i in range(1, 20)], [float(i) for i in f1s], bar_width, alpha=0.8, color='g', label='F1 Score')
    plt.xticks([i for i in range(1, 20)])
    plt.legend()
    plt.xlabel('Accuracies/F1 Scores')
    plt.ylabel('Value')
    plt.show()

def graphKNNVersusTree(knn_perf, tree_perf):
    labels = ['Accuracy', 'Precision', 'Recall', 'F1 Score']
    index = np.arange(len(labels))
    bar_width = 0.35
    opacity = 0.8
    plt.bar(index, [float(i) for i in knn_perf.values()], bar_width, alpha=opacity, color='b', label='KNN')
    plt.bar(index + bar_width, [float(i) for i in tree_perf.values()], bar_width, alpha=opacity, color='g', label='Tree')
    plt.xticks(index + (bar_width / 2), ('Accuracy', 'Precision', 'Recall', 'F1 Score'))
    plt.title('KNN vs Decision Tree Performance')
    plt.legend()
    plt.show()

def main():
    createPartitions()

    # Train data mining tasks
    train.trainTree(train_partition_filename)

    # Test data mining tasks
    tree_predict, tree_expected = test.testTree(test_partition_filename)

    print("\n ---- Tree Performance ---- \n")
    tree_perf = calculatePerformance(tree_predict, tree_expected)
    print(tree_perf)
    print("\n ---- KNN Performance (K = 8) ---- \n")
    train.trainKNN(train_partition_filename, 8)
    knn_predict, knn_expected = test.testKNN(test_partition_filename)
    knn_perf = calculatePerformance(knn_predict, knn_expected)
    print(knn_perf)
    graphKNN()
    graphKNNVersusTree(knn_perf, tree_perf)

    
if __name__ == "__main__":
    main()
