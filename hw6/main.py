from surprise import Dataset
from surprise import Reader
from surprise import SVD, NMF, KNNBasic
from surprise import evaluate, print_perf
import matplotlib.pyplot as plt
import numpy as np
import os

file_path = os.path.expanduser('restaurant_ratings.txt')
reader = Reader(line_format='user item rating timestamp', sep='\t')
data = Dataset.load_from_file(file_path, reader=reader)

data.split(n_folds=3)

# SVD
print("\nSVD")
algo = SVD()
perf = evaluate(algo, data, measures=['RMSE', 'MAE'], verbose=0)
print_perf(perf)

print("\nPMF")
# PMF
algo = SVD(biased=False)
perf = evaluate(algo, data, measures=['RMSE', 'MAE'], verbose=0)
print_perf(perf)

print("\nNMF")
# NMF
algo = NMF()
perf = evaluate(algo, data, measures=['RMSE', 'MAE'], verbose=0)
print_perf(perf)

print("\nUser Collaberative Filtering")
# User Collaberative Filtering
algo = KNNBasic(sim_options = {'user_based': True })
perf = evaluate(algo, data, measures=['RMSE', 'MAE'], verbose=0)
print_perf(perf)

print("\nItem Collaberative Filtering")
# Item Collaberative Filtering
algo = KNNBasic(sim_options = {'user_based': False })
perf = evaluate(algo, data, measures=['RMSE', 'MAE'], verbose=0)
print_perf(perf)

#### User based Comparison

def mean(l):
    return sum(l)/len(l)

N = 3
ind = np.arange(N)
bar_width = .35

rmse_data = []
mae_data = []

print("\nMean Squared Difference")
# Mean Squared Difference
algo = KNNBasic(sim_options = {'name':"MSD",'user_based': True })
perf = evaluate(algo, data, measures=['RMSE', 'MAE'], verbose=0)
print_perf(perf)
rmse_data.append(mean(perf['rmse']))
mae_data.append(mean(perf['mae']))

# plt.bar(index, data, bar_width)

print("\nCosine Similarity Collaberative Filtering")
# Cosine Similarity Collaberative Filtering
algo = KNNBasic(sim_options = {'name':"cosine",'user_based': True })
perf = evaluate(algo, data, measures=['RMSE', 'MAE'], verbose=0)
print_perf(perf)
rmse_data.append(mean(perf['rmse']))
mae_data.append(mean(perf['mae']))

print("\nPearson Collaberative Filtering")
algo = KNNBasic(sim_options = {'name':"pearson",'user_based': True }) 
perf = evaluate(algo, data, measures=['RMSE', 'MAE'], verbose=0)
print_perf(perf)
rmse_data.append(mean(perf['rmse']))
mae_data.append(mean(perf['mae']))

x = ('MSD', 'Cosine', 'Pearson')
# x_pos = [i for i, _ in enumerate(x)]
plt.bar(ind, rmse_data, width=bar_width, color="green", label="RMSE")
plt.bar(ind + bar_width, mae_data, width=bar_width, color="blue", label="MAE")

plt.xticks(ind + bar_width / 2, x)
plt.legend(loc='best')
plt.title('MAE vs RMSE User Collaberative Filtering')
plt.show()

### Item Based Comparison

N = 3
ind = np.arange(N)
bar_width = .35

rmse_data = []
mae_data = []

print("\nMean Squared Difference")
# Mean Squared Difference
algo = KNNBasic(sim_options = {'name':"MSD",'user_based': False })
perf = evaluate(algo, data, measures=['RMSE', 'MAE'], verbose=0)
print_perf(perf)
rmse_data.append(mean(perf['rmse']))
mae_data.append(mean(perf['mae']))

print("\nCosine Similarity Collaberative Filtering")
# Cosine Similarity Collaberative Filtering
algo = KNNBasic(sim_options = {'name':"cosine",'user_based': False })
perf = evaluate(algo, data, measures=['RMSE', 'MAE'], verbose=0)
print_perf(perf)
rmse_data.append(mean(perf['rmse']))
mae_data.append(mean(perf['mae']))

print("\nCosine Similarity Collaberative Filtering")
algo = KNNBasic(sim_options = {'name':"pearson",'user_based': False }) 
perf = evaluate(algo, data, measures=['RMSE', 'MAE'], verbose=0)
print_perf(perf)

rmse_data.append(mean(perf['rmse']))
mae_data.append(mean(perf['mae']))

x = ('MSD', 'Cosine', 'Pearson')
# x_pos = [i for i, _ in enumerate(x)]
plt.bar(ind, rmse_data, width=bar_width, color="green", label="RMSE")
plt.bar(ind + bar_width, mae_data, width=bar_width, color="blue", label="MAE")

plt.xticks(ind + bar_width / 2, x)
plt.legend(loc='best')
plt.title('MAE vs RMSE Item Collaberative Filtering')
plt.show()

#### More K

# user_data = []
# item_data = []

# N = 20
# ind = np.arange(N)
# bar_width = .35

# for i in range(1, 21):
#     algo = KNNBasic(k=i, sim_options = {'name':"MSD",'user_based': True })
#     perf = evaluate(algo, data, measures=['RMSE'], verbose=0)
#     user_data.append(mean(perf['rmse']))

#     algo = KNNBasic(k=i, sim_options = {'name':"MSD", 'user_based': False })
#     perf = evaluate(algo, data, measures=['RMSE'], verbose=0)
#     print_perf(perf)
#     item_data.append(mean(perf['rmse']))

# x = [i for i in range(1, 21)]
# # x_pos = [i for i, _ in enumerate(x)]
# plt.bar(ind, user_data, width=bar_width, color="green", label="User")
# plt.bar(ind + bar_width, item_data, width=bar_width, color="blue", label="Item")

# plt.xticks(ind + bar_width / 2, x)
# plt.legend(loc='best')
# plt.title('User vs Item Collaberative Filtering with different k')
# plt.show()