
# coding: utf-8

# In[44]:


from sklearn import tree
import graphviz
import pandas as pd
import numpy as np
import c45

df = pd.read_csv("./data/train.4.1.csv")


# # 4.1 Gini Index Tree (Manual Calculation)

# In[2]:


import math
def gini(labels):
    l = labels.value_counts().to_dict().values()
    return 1 - sum([(value / sum(l))**2 for value in l])

print('Gini before:', gini(df['Label: Win/Lose']))
print('Split on Home/away')
home_split = df.loc[df['Is Home/Away?'] == 0]
away_split = df.loc[df['Is Home/Away?'] == 1]
print('Home split node gini:', gini(home_split['Label: Win/Lose']))
print('Away split node gini:', gini(away_split['Label: Win/Lose']))

print('Split on in/out')
in_split = df.loc[df['Is Opponent in AP Top 25 at Preseason?'] == 1]
out_split = df.loc[df['Is Opponent in AP Top 25 at Preseason?'] == 0]
print('In split node gini:', gini(in_split['Label: Win/Lose']))
print('Out split node gini:', gini(out_split['Label: Win/Lose']))

print('Split on in/out because gini index is 0 for both nodes')


# ![4.1.png](attachment:4.1.png)

# # 4.1 Gini Index Tree (SKLearn)

# In[3]:


x = df.drop('Label: Win/Lose', axis=1)
y = df['Label: Win/Lose']


# In[4]:


clf = tree.DecisionTreeClassifier()
clf = clf.fit(x,y)
dot_data = tree.export_graphviz(clf, out_file=None)
graph = graphviz.Source(dot_data)
graph.render('4.1')
graph


# # 4.1 Entropy Tree (Manual Calculation)

# In[5]:


def entropy(labels):
    l = labels.value_counts().to_dict().values()
    return -sum([((value / sum(l)) * math.log2(value / sum(l))) for value in l])

print('Entropy before:', entropy(df['Label: Win/Lose']))
print('Split on Home/away')
home_split = df.loc[df['Is Home/Away?'] == 0]
away_split = df.loc[df['Is Home/Away?'] == 1]
print('Home split node entropy:', entropy(home_split['Label: Win/Lose']))
print('Away split node entropy:', entropy(away_split['Label: Win/Lose']))

print('Split on in/out')
in_split = df.loc[df['Is Opponent in AP Top 25 at Preseason?'] == 1]
out_split = df.loc[df['Is Opponent in AP Top 25 at Preseason?'] == 0]
print('In split node entropy:', entropy(in_split['Label: Win/Lose']))
print('Out split node entropy:', entropy(out_split['Label: Win/Lose']))

print('Split on in/out because entropy is 0 for both nodes in split')


# ![4.1.png](attachment:4.1.png)

# # 4.1 Entropy Tree (SKLearn)

# In[6]:


clf = tree.DecisionTreeClassifier(criterion='entropy')
clf = clf.fit(x,y)
dot_data = tree.export_graphviz(clf, out_file=None)
graph = graphviz.Source(dot_data)
graph.render('4.1')
graph


# # 4.1 Gain Ratio Tree (Manual Calculation)

# In[74]:


def gain(before, after):
    return entropy(before) - sum([(len(i)/len(before))*entropy(i) for i in after])

def gainratio(splits, size):
    lengths = []
    for i in splits:
        if (len(i) != 0):
            lengths.append(len(i))
    return -sum([(x/size)*math.log2(x/size) for x in lengths])

print('Entropy before:', entropy(df['Label: Win/Lose']))
print('Split on Home/away')
home_split = df.loc[df['Is Home/Away?'] == 0]
away_split = df.loc[df['Is Home/Away?'] == 1]

home_away_ratio = gain(df['Label: Win/Lose'], [home_split['Label: Win/Lose'], away_split['Label: Win/Lose']]) / gainratio([home_split, away_split], len(df))

print('Home-away split gain ratio:', home_away_ratio)

print('Split on in/out')
in_split = df.loc[df['Is Opponent in AP Top 25 at Preseason?'] == 1]
out_split = df.loc[df['Is Opponent in AP Top 25 at Preseason?'] == 0]

in_out_ratio = gain(df['Label: Win/Lose'], [in_split['Label: Win/Lose'], out_split['Label: Win/Lose']]) / gainratio([in_split, out_split], len(df))

print('In-out split gain ratio:', in_out_ratio)

print('Split on in/out because gain ratio is 1 which is maximum for the split')


# ![4.1.png](attachment:4.1.png)

# # 4.1 Gain Ration Tree (Github Library)

# In[36]:


c45Labels = ['HomeOrAway','Opponent In Top 25', 'Media']
c45Tree = c45.create_tree(df.values.tolist(), c45Labels)
print(c45Tree)


# ![4.1.png](attachment:4.1.png)

# # Task 4.2 Gini Index Tree (Manual Calculated)

# In[45]:


df2 = pd.read_csv('./data/train.4.2.csv')


# In[8]:


print('Gini before:', gini(df2['Label: Play?']))
print('Split on Outlook?')
sunny_split = df2.loc[df2['Outlook'] == 1]
overcast_split = df2.loc[df2['Outlook'] == 2]
rainy_split = df2.loc[df2['Outlook'] == 3]
print('Sunny split node gini:', gini(sunny_split['Label: Play?']))
print('Overcast split node gini:', gini(overcast_split['Label: Play?']))
print('Rainy split node gini:', gini(rainy_split['Label: Play?']))
print('Gini on this split:',(len(sunny_split)/len(df2))*gini(sunny_split['Label: Play?']) + (len(overcast_split)/len(df2))*gini(overcast_split['Label: Play?']) + (len(rainy_split)/len(df2))*gini(rainy_split['Label: Play?']))

print('\nSplit on Temperature?')
hot_split = df2.loc[df2['Temperature'] == 1]
mild_split = df2.loc[df2['Temperature'] == 2]
cool_split = df2.loc[df2['Temperature'] == 3]
print('Hot split node gini:', gini(hot_split['Label: Play?']))
print('Mild split node gini:', gini(mild_split['Label: Play?']))
print('Cool split node gini:', gini(cool_split['Label: Play?']))
print('Gini on this split:',(len(hot_split)/len(df2))*gini(hot_split['Label: Play?']) + (len(mild_split)/len(df2))*gini(mild_split['Label: Play?']) + (len(cool_split)/len(df2))*gini(cool_split['Label: Play?']))

print('\nSplit on Humidity?')
high_split = df2.loc[df2['Humidity'] == 0]
norm_split = df2.loc[df2['Humidity'] == 1]
print('high split node gini:', gini(high_split['Label: Play?']))
print('norm split node gini:', gini(norm_split['Label: Play?']))
print('Gini on this split:',(len(high_split)/len(df2))*gini(high_split['Label: Play?']) + (len(norm_split)/len(df2))*gini(norm_split['Label: Play?']))

print('\nSplit on Windy?')
true_split = df2.loc[df2['Windy'] == 0]
false_split = df2.loc[df2['Windy'] == 1]
print('true split node gini:', gini(true_split['Label: Play?']))
print('false split node gini:', gini(false_split['Label: Play?']))
print('Gini on this split:',(len(true_split)/len(df2))*gini(true_split['Label: Play?']) + (len(false_split)/len(df2))*gini(false_split['Label: Play?']))

print('\nSplit on Outlook')


# In[37]:


print('overcast node split?')
print('\nSplit on Temperature?')
overcast_node = df2.loc[df2['Outlook'] == 2]
hot_split = overcast_node.loc[overcast_node['Temperature'] == 1]
mild_split = overcast_node.loc[overcast_node['Temperature'] == 2]
cool_split = overcast_node.loc[overcast_node['Temperature'] == 3]
print('Hot split node gini:', gini(hot_split['Label: Play?']))
print('Mild split node gini:', gini(mild_split['Label: Play?']))
print('Cool split node gini:', gini(cool_split['Label: Play?']))
print('Gini on this split:',(len(hot_split)/len(df2))*gini(hot_split['Label: Play?']) + (len(mild_split)/len(df2))*gini(mild_split['Label: Play?']) + (len(cool_split)/len(df2))*gini(cool_split['Label: Play?']))

print('\nSplit on Humidity?')
high_split = overcast_node.loc[overcast_node['Humidity'] == 0]
norm_split = overcast_node.loc[overcast_node['Humidity'] == 1]
print('high split node gini:', gini(high_split['Label: Play?']))
print('norm split node gini:', gini(norm_split['Label: Play?']))
print('Gini on this split:',(len(high_split)/len(df2))*gini(high_split['Label: Play?']) + (len(norm_split)/len(df2))*gini(norm_split['Label: Play?']))

print('\nSplit on Windy?')
true_split = overcast_node.loc[overcast_node['Windy'] == 0]
false_split = overcast_node.loc[overcast_node['Windy'] == 1]
print('true split node gini:', gini(true_split['Label: Play?']))
print('false split node gini:', gini(false_split['Label: Play?']))
print('Gini on this split:',(len(true_split)/len(df2))*gini(true_split['Label: Play?']) + (len(false_split)/len(df2))*gini(false_split['Label: Play?']))

print('split on humidity because 0 gini and it is only binary')
print(overcast_node)


# In[38]:


print('Rainy node split?')
print('\nSplit on Temperature?')
rainy_node = df2.loc[df2['Outlook'] == 3]
hot_split = rainy_node.loc[rainy_node['Temperature'] == 1]
mild_split = rainy_node.loc[rainy_node['Temperature'] == 2]
cool_split = rainy_node.loc[rainy_node['Temperature'] == 3]
print('Hot split node gini:', gini(hot_split['Label: Play?']))
print('Mild split node gini:', gini(mild_split['Label: Play?']))
print('Cool split node gini:', gini(cool_split['Label: Play?']))
print('Gini on this split:',(len(hot_split)/len(df2))*gini(hot_split['Label: Play?']) + (len(mild_split)/len(df2))*gini(mild_split['Label: Play?']) + (len(cool_split)/len(df2))*gini(cool_split['Label: Play?']))

print('\nSplit on Humidity?')
high_split = rainy_node.loc[rainy_node['Humidity'] == 0]
norm_split = rainy_node.loc[rainy_node['Humidity'] == 1]
print('high split node gini:', gini(high_split['Label: Play?']))
print('norm split node gini:', gini(norm_split['Label: Play?']))
print('Gini on this split:',(len(high_split)/len(df2))*gini(high_split['Label: Play?']) + (len(norm_split)/len(df2))*gini(norm_split['Label: Play?']))

print('\nSplit on Windy?')
true_split = rainy_node.loc[rainy_node['Windy'] == 0]
false_split = rainy_node.loc[rainy_node['Windy'] == 1]
print('true split node gini:', gini(true_split['Label: Play?']))
print('false split node gini:', gini(false_split['Label: Play?']))
print('Gini on this split:',(len(true_split)/len(df2))*gini(true_split['Label: Play?']) + (len(false_split)/len(df2))*gini(false_split['Label: Play?']))
print('Split sunny node on humidity, 0 gini so both splits are labels')

print('split on windy since there is 0 gini')
print(rainy_node)


# In[39]:


print('sunny node split?')
print('\nSplit on Temperature?')
sunny_node = df2.loc[df2['Outlook'] == 1]
hot_split = sunny_node.loc[sunny_node['Temperature'] == 1]
mild_split = sunny_node.loc[sunny_node['Temperature'] == 2]
cool_split = sunny_node.loc[sunny_node['Temperature'] == 3]
print('Hot split node gini:', gini(hot_split['Label: Play?']))
print('Mild split node gini:', gini(mild_split['Label: Play?']))
print('Cool split node gini:', gini(cool_split['Label: Play?']))
print('Gini on this split:',(len(hot_split)/len(df2))*gini(hot_split['Label: Play?']) + (len(mild_split)/len(df2))*gini(mild_split['Label: Play?']) + (len(cool_split)/len(df2))*gini(cool_split['Label: Play?']))

print('\nSplit on Humidity?')
high_split = sunny_node.loc[sunny_node['Humidity'] == 0]
norm_split = sunny_node.loc[sunny_node['Humidity'] == 1]
print('high split node gini:', gini(high_split['Label: Play?']))
print('norm split node gini:', gini(norm_split['Label: Play?']))
print('Gini on this split:',(len(high_split)/len(df2))*gini(high_split['Label: Play?']) + (len(norm_split)/len(df2))*gini(norm_split['Label: Play?']))

print('\nSplit on Windy?')
true_split = sunny_node.loc[sunny_node['Windy'] == 0]
false_split = sunny_node.loc[sunny_node['Windy'] == 1]
print('true split node gini:', gini(true_split['Label: Play?']))
print('false split node gini:', gini(false_split['Label: Play?']))
print('Gini on this split:',(len(true_split)/len(df2))*gini(true_split['Label: Play?']) + (len(false_split)/len(df2))*entropy(false_split['Label: Play?']))
print('Split sunny node on humidity, 0 gini so both splits are labels')
print(sunny_node)


# ![4.2_handdrawn.png](attachment:4.2_handdrawn.png)

# In[12]:


v = df2.drop('Label: Play?', axis=1)
w = df2['Label: Play?']


# # 4.2 Gini Index Tree (SKLearn)

# In[13]:


clf = tree.DecisionTreeClassifier()
clf = clf.fit(v,w)
dot_data = tree.export_graphviz(clf, out_file=None)
graph = graphviz.Source(dot_data)
graph.render('4.2')
graph


# # 4.2 Entropy Tree (Manual Calculation)

# In[14]:


print('Entropy before:', entropy(df2['Label: Play?']))
print('Split on Outlook?')
sunny_split = df2.loc[df2['Outlook'] == 1]
overcast_split = df2.loc[df2['Outlook'] == 2]
rainy_split = df2.loc[df2['Outlook'] == 3]
print('Sunny split node entropy:', entropy(sunny_split['Label: Play?']))
print('Overcast split node entropy:', entropy(overcast_split['Label: Play?']))
print('Rainy split node entropy:', entropy(rainy_split['Label: Play?']))
print('Entropy on this split:',(len(sunny_split)/len(df2))*entropy(sunny_split['Label: Play?']) + (len(overcast_split)/len(df2))*entropy(overcast_split['Label: Play?']) + (len(rainy_split)/len(df2))*entropy(rainy_split['Label: Play?']))

print('\nSplit on Temperature?')
hot_split = df2.loc[df2['Temperature'] == 1]
mild_split = df2.loc[df2['Temperature'] == 2]
cool_split = df2.loc[df2['Temperature'] == 3]
print('Hot split node entropy:', entropy(hot_split['Label: Play?']))
print('Mild split node entropy:', entropy(mild_split['Label: Play?']))
print('Cool split node entropy:', entropy(cool_split['Label: Play?']))
print('Entropy on this split:',(len(hot_split)/len(df2))*entropy(hot_split['Label: Play?']) + (len(mild_split)/len(df2))*entropy(mild_split['Label: Play?']) + (len(cool_split)/len(df2))*entropy(cool_split['Label: Play?']))

print('\nSplit on Humidity?')
high_split = df2.loc[df2['Humidity'] == 0]
norm_split = df2.loc[df2['Humidity'] == 1]
print('high split node entropy:', entropy(high_split['Label: Play?']))
print('norm split node entropy:', entropy(norm_split['Label: Play?']))
print('Entropy on this split:',(len(high_split)/len(df2))*entropy(high_split['Label: Play?']) + (len(norm_split)/len(df2))*entropy(norm_split['Label: Play?']))

print('\nSplit on Windy?')
true_split = df2.loc[df2['Windy'] == 0]
false_split = df2.loc[df2['Windy'] == 1]
print('true split node entropy:', entropy(true_split['Label: Play?']))
print('false split node entropy:', entropy(false_split['Label: Play?']))
print('Entropy on this split:',(len(true_split)/len(df2))*entropy(true_split['Label: Play?']) + (len(false_split)/len(df2))*entropy(false_split['Label: Play?']))

print('Split on outlook because it has the lowest entropy')


# In[15]:


print('overcast node split?')
print('\nSplit on Temperature?')
overcast_node = df2.loc[df2['Outlook'] == 2]
hot_split = overcast_node.loc[overcast_node['Temperature'] == 1]
mild_split = overcast_node.loc[overcast_node['Temperature'] == 2]
cool_split = overcast_node.loc[overcast_node['Temperature'] == 3]
print('Hot split node entropy:', entropy(hot_split['Label: Play?']))
print('Mild split node entropy:', entropy(mild_split['Label: Play?']))
print('Cool split node entropy:', entropy(cool_split['Label: Play?']))
print('entropy on this split:',(len(hot_split)/len(df2))*entropy(hot_split['Label: Play?']) + (len(mild_split)/len(df2))*entropy(mild_split['Label: Play?']) + (len(cool_split)/len(df2))*entropy(cool_split['Label: Play?']))

print('\nSplit on Humidity?')
high_split = overcast_node.loc[overcast_node['Humidity'] == 0]
norm_split = overcast_node.loc[overcast_node['Humidity'] == 1]
print('high split node entropy:', entropy(high_split['Label: Play?']))
print('norm split node entropy:', entropy(norm_split['Label: Play?']))
print('entropy on this split:',(len(high_split)/len(df2))*entropy(high_split['Label: Play?']) + (len(norm_split)/len(df2))*entropy(norm_split['Label: Play?']))

print('\nSplit on Windy?')
true_split = overcast_node.loc[overcast_node['Windy'] == 0]
false_split = overcast_node.loc[overcast_node['Windy'] == 1]
print('true split node entropy:', entropy(true_split['Label: Play?']))
print('false split node entropy:', entropy(false_split['Label: Play?']))
print('entropy on this split:',(len(true_split)/len(df2))*entropy(true_split['Label: Play?']) + (len(false_split)/len(df2))*entropy(false_split['Label: Play?']))

print('split on humidity because 0 entropy and it is only binary')


# In[16]:


print('Rainy node split?')
print('\nSplit on Temperature?')
rainy_node = df2.loc[df2['Outlook'] == 3]
hot_split = rainy_node.loc[rainy_node['Temperature'] == 1]
mild_split = rainy_node.loc[rainy_node['Temperature'] == 2]
cool_split = rainy_node.loc[rainy_node['Temperature'] == 3]
print('Hot split node entropy:', entropy(hot_split['Label: Play?']))
print('Mild split node entropy:', entropy(mild_split['Label: Play?']))
print('Cool split node entropy:', entropy(cool_split['Label: Play?']))
print('entropy on this split:',(len(hot_split)/len(df2))*entropy(hot_split['Label: Play?']) + (len(mild_split)/len(df2))*entropy(mild_split['Label: Play?']) + (len(cool_split)/len(df2))*entropy(cool_split['Label: Play?']))

print('\nSplit on Humidity?')
high_split = rainy_node.loc[rainy_node['Humidity'] == 0]
norm_split = rainy_node.loc[rainy_node['Humidity'] == 1]
print('high split node entropy:', entropy(high_split['Label: Play?']))
print('norm split node entropy:', entropy(norm_split['Label: Play?']))
print('entropy on this split:',(len(high_split)/len(df2))*entropy(high_split['Label: Play?']) + (len(norm_split)/len(df2))*entropy(norm_split['Label: Play?']))

print('\nSplit on Windy?')
true_split = rainy_node.loc[rainy_node['Windy'] == 0]
false_split = rainy_node.loc[rainy_node['Windy'] == 1]
print('true split node entropy:', entropy(true_split['Label: Play?']))
print('false split node entropy:', entropy(false_split['Label: Play?']))
print('entropy on this split:',(len(true_split)/len(df2))*entropy(true_split['Label: Play?']) + (len(false_split)/len(df2))*entropy(false_split['Label: Play?']))
print('Split sunny node on humidity, 0 entropy so both splits are labels')

print('split on windy since there is 0 entropy')


# In[17]:


print('sunny node split?')
print('\nSplit on Temperature?')
sunny_node = df2.loc[df2['Outlook'] == 1]
hot_split = sunny_node.loc[sunny_node['Temperature'] == 1]
mild_split = sunny_node.loc[sunny_node['Temperature'] == 2]
cool_split = sunny_node.loc[sunny_node['Temperature'] == 3]
print('Hot split node entropy:', entropy(hot_split['Label: Play?']))
print('Mild split node entropy:', entropy(mild_split['Label: Play?']))
print('Cool split node entropy:', entropy(cool_split['Label: Play?']))
print('entropy on this split:',(len(hot_split)/len(df2))*entropy(hot_split['Label: Play?']) + (len(mild_split)/len(df2))*entropy(mild_split['Label: Play?']) + (len(cool_split)/len(df2))*entropy(cool_split['Label: Play?']))

print('\nSplit on Humidity?')
high_split = sunny_node.loc[sunny_node['Humidity'] == 0]
norm_split = sunny_node.loc[sunny_node['Humidity'] == 1]
print('high split node entropy:', entropy(high_split['Label: Play?']))
print('norm split node entropy:', entropy(norm_split['Label: Play?']))
print('entropy on this split:',(len(high_split)/len(df2))*entropy(high_split['Label: Play?']) + (len(norm_split)/len(df2))*entropy(norm_split['Label: Play?']))

print('\nSplit on Windy?')
true_split = sunny_node.loc[sunny_node['Windy'] == 0]
false_split = sunny_node.loc[sunny_node['Windy'] == 1]
print('true split node entropy:', entropy(true_split['Label: Play?']))
print('false split node entropy:', entropy(false_split['Label: Play?']))
print('entropy on this split:',(len(true_split)/len(df2))*entropy(true_split['Label: Play?']) + (len(false_split)/len(df2))*entropy(false_split['Label: Play?']))
print('Split sunny node on humidity, 0 gini so both splits are labels')


# ![4.2_handdrawn.png](attachment:4.2_handdrawn.png)

# # 4.2 Entropy Tree (SKLearn)

# In[18]:


clf = tree.DecisionTreeClassifier(criterion='entropy')
clf = clf.fit(v,w)
dot_data = tree.export_graphviz(clf, out_file=None)
graph = graphviz.Source(dot_data)
graph.render('4.2')
graph


# # 4.2 Gain Ratio Tree (Manual Calculation)

# In[59]:


print('Entropy before:', entropy(df2['Label: Play?']))
print('Split on Outlook?')
sunny_split = df2.loc[df2['Outlook'] == 1]
overcast_split = df2.loc[df2['Outlook'] == 2]
rainy_split = df2.loc[df2['Outlook'] == 3]

outlook_ratio = gain(df2['Label: Play?'], [sunny_split['Label: Play?'], overcast_split['Label: Play?'], rainy_split['Label: Play?']]) / gainratio([sunny_split, rainy_split, overcast_split], len(df2))

print('Outlook gain ratio:', outlook_ratio)

print('\nSplit on Temperature?')
hot_split = df2.loc[df2['Temperature'] == 1]
mild_split = df2.loc[df2['Temperature'] == 2]
cool_split = df2.loc[df2['Temperature'] == 3]

temp_ratio = gain(df2['Label: Play?'], [hot_split['Label: Play?'], mild_split['Label: Play?'], cool_split['Label: Play?']]) / gainratio([cool_split, mild_split, hot_split], len(df2))

print('Temperature gain ratio:', temp_ratio)


print('\nSplit on Humidity?')
high_split = df2.loc[df2['Humidity'] == 0]
norm_split = df2.loc[df2['Humidity'] == 1]

humidity_ratio = gain(df2['Label: Play?'], [high_split['Label: Play?'], norm_split['Label: Play?']]) / gainratio([high_split, norm_split], len(df2))

print('Humidity gain ratio:', humidity_ratio)

print('\nSplit on Windy?')
true_split = df2.loc[df2['Windy'] == 0]
false_split = df2.loc[df2['Windy'] == 1]
windy_ratio = gain(df2['Label: Play?'], [true_split['Label: Play?'], false_split['Label: Play?']]) / gainratio([true_split, false_split], len(df2))

print('Windy gain ratio:', windy_ratio)

print('Split on outlook because it has the highest gain ratio')


# In[62]:


print('overcast node split?')
print('\nSplit on Temperature?')
overcast_node = df2.loc[df2['Outlook'] == 2]
hot_split = overcast_node.loc[overcast_node['Temperature'] == 1]
mild_split = overcast_node.loc[overcast_node['Temperature'] == 2]
cool_split = overcast_node.loc[overcast_node['Temperature'] == 3]

outlook_ratio = gain(overcast_node['Label: Play?'], [hot_split['Label: Play?'], mild_split['Label: Play?'], cool_split['Label: Play?']]) / gainratio([hot_split, mild_split, cool_split], len(overcast_node))

print('Outlook gain ratio:', outlook_ratio)

print('\nSplit on Humidity?')
high_split = overcast_node.loc[overcast_node['Humidity'] == 0]
norm_split = overcast_node.loc[overcast_node['Humidity'] == 1]

humidity_ratio = gain(overcast_node['Label: Play?'], [high_split['Label: Play?'], norm_split['Label: Play?']]) / gainratio([high_split, norm_split], len(overcast_node))

print('Humidity gain ratio:', humidity_ratio)

print('\nSplit on Windy?')
true_split = overcast_node.loc[overcast_node['Windy'] == 0]
false_split = overcast_node.loc[overcast_node['Windy'] == 1]

windy_ratio = gain(overcast_node['Label: Play?'], [true_split['Label: Play?'], false_split['Label: Play?']]) / gainratio([true_split, false_split], len(overcast_node))

print('Windy gain ratio:', windy_ratio)

print('split on humidity because 0 gain ratio and it is only binary')


# In[75]:


print('Rainy node split?')
print('\nSplit on Temperature?')
rainy_node = df2.loc[df2['Outlook'] == 3]
hot_split = rainy_node.loc[rainy_node['Temperature'] == 1]
mild_split = rainy_node.loc[rainy_node['Temperature'] == 2]
cool_split = rainy_node.loc[rainy_node['Temperature'] == 3]
print(len(hot_split))
print(len(mild_split))
print(len(cool_split))
outlook_ratio = gain(rainy_node['Label: Play?'], [hot_split['Label: Play?'], mild_split['Label: Play?'], cool_split['Label: Play?']]) / gainratio([hot_split, mild_split, cool_split], len(rainy_node))

print('Outlook gain ratio:', outlook_ratio)

print('\nSplit on Humidity?')
high_split = rainy_node.loc[rainy_node['Humidity'] == 0]
norm_split = rainy_node.loc[rainy_node['Humidity'] == 1]

humidity_ratio = gain(rainy_node['Label: Play?'], [high_split['Label: Play?'], norm_split['Label: Play?']]) / gainratio([high_split, norm_split], len(rainy_node))

print('Humidity gain ratio:', humidity_ratio)

print('\nSplit on Windy?')
true_split = rainy_node.loc[rainy_node['Windy'] == 0]
false_split = rainy_node.loc[rainy_node['Windy'] == 1]

windy_ratio = gain(rainy_node['Label: Play?'], [true_split['Label: Play?'], false_split['Label: Play?']]) / gainratio([true_split, false_split], len(rainy_node))

print('Windy gain ratio:', windy_ratio)

print('split on windy since gain ratio is 1 which is max')


# In[78]:


print('sunny node split?')
print('\nSplit on Temperature?')
sunny_node = df2.loc[df2['Outlook'] == 1]
hot_split = sunny_node.loc[sunny_node['Temperature'] == 1]
mild_split = sunny_node.loc[sunny_node['Temperature'] == 2]
cool_split = sunny_node.loc[sunny_node['Temperature'] == 3]

outlook_ratio = gain(sunny_node['Label: Play?'], [hot_split['Label: Play?'], mild_split['Label: Play?'], cool_split['Label: Play?']]) / gainratio([hot_split, mild_split, cool_split], len(sunny_node))

print('Outlook gain ratio:', outlook_ratio)

print('\nSplit on Humidity?')
high_split = sunny_node.loc[sunny_node['Humidity'] == 0]
norm_split = sunny_node.loc[sunny_node['Humidity'] == 1]

humidity_ratio = gain(sunny_node['Label: Play?'], [high_split['Label: Play?'], norm_split['Label: Play?']]) / gainratio([high_split, norm_split], len(sunny_node))

print('Humidity gain ratio:', humidity_ratio)

print('\nSplit on Windy?')
true_split = sunny_node.loc[sunny_node['Windy'] == 0]
false_split = sunny_node.loc[sunny_node['Windy'] == 1]

windy_ratio = gain(sunny_node['Label: Play?'], [true_split['Label: Play?'], false_split['Label: Play?']]) / gainratio([true_split, false_split], len(sunny_node))

print('Windy gain ratio:', windy_ratio)

print('Split sunny node on humidity because gain ratio is 1 which is max')


# ![4.2_handdrawn.png](attachment:4.2_handdrawn.png)

# # 4.2 Gain Ratio Tree (Github c45 library)

# In[33]:


c45Labels = ['Outlook','Temperature','Humidity','Windy']
c45Tree = c45.create_tree(df2.values.tolist(), c45Labels)
print(c45Tree)


# ![4.2.png](attachment:4.2.png)

# # Task 5

# In[46]:


df3 = pd.read_csv('./data/train.5.csv')


# In[20]:


a = df3.drop('Label', axis=1)
b = df3['Label']


# # 5 Entropy Tree

# In[21]:


clf = tree.DecisionTreeClassifier(criterion='entropy')
clf = clf.fit(a,b)
dot_data = tree.export_graphviz(clf, out_file=None)
graph = graphviz.Source(dot_data)
graph.render('5')
graph


# In[40]:


test = pd.read_csv('./data/test.5.csv')
test.head()


# # 5 Accuracy, Precision, Recall, and F1 Calculations for Entropy Tree

# In[79]:


no_label = train.drop('Label', axis=1)
output = clf.predict(no_label)
expected = test['Label'].tolist()
print(output)
tp = 0
fp = 0
tn = 0
fn = 0

for i in range(0, len(expected)):
    if output[i] == 1 and expected[i] == 1:
        tp += 1
    if output[i] == 1 and expected[i] == 0:
        fp += 1
    if output[i] == 0 and expected[i] == 0:
        tn += 1
    if output[i] == 0 and expected[i] == 1:
        fn += 1
    
recall = (tp/(tp + fn))
precision = (tp/(tp + fp))
        
print('Accuray:', ((tp + tn)/(tp + tn + fp + fn))*100, '%')
print('Precision:', precision*100, '%')
print('Recall:', recall*100, '%')
print('F1:', ((2*(recall*precision))/(recall+precision))*100, '%')


# # 5 Gain Ratio Tree

# In[42]:


c45Labels = ['HomeOrAway','Opponent In Top 25', 'Media']
c45Tree = c45.create_tree(df3.values.tolist(), c45Labels)
print(c45Tree)


# ![5.png](attachment:5.png)

# # 5 Accuracy, Precision, Recall, and F1 Calculations for Gain Ratio

# In[43]:


no_label = test.drop('Label', axis=1)
output = [1, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0]
expected = test['Label'].tolist()

tp = 0
fp = 0
tn = 0
fn = 0

for i in range(0, len(expected)):
    if output[i] == 1 and expected[i] == 1:
        tp += 1
    if output[i] == 1 and expected[i] == 0:
        fp += 1
    if output[i] == 0 and expected[i] == 0:
        tn += 1
    if output[i] == 0 and expected[i] == 1:
        fn += 1
    
recall = (tp/(tp + fn))
precision = (tp/(tp + fp))
        
print('Accuray:', ((tp + tn)/(tp + tn + fp + fn))*100, '%')
print('Precision:', precision*100, '%')
print('Recall:', recall*100, '%')
print('F1:', ((2*(recall*precision))/(recall+precision))*100, '%')

