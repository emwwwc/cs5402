import math
import random

def loadCSV(fileName):
    fileHandler = open(fileName, "rt")
    lines = fileHandler.readlines()
    fileHandler.close()
    del lines[0] # remove the header
    dataset = []
    labels = []
    for line in lines:
        instance, label = lineToTuple(line)
        dataset.append(instance)
        labels.append(label)
    return dataset, labels

def lineToTuple(line):
    # remove leading/trailing witespace and newlines
    cleanLine = line.strip()
    # get rid of quotes
    cleanLine = cleanLine.replace('"', '')
    # separate the fields
    lineList = cleanLine.split(",")
    label = lineList[-1]
    # remove label
    lineList = lineList[:-1]
    # convert strings into numbers
    lineList = [float(i) for i in lineList]
    lineTuple = tuple(lineList)
    return lineTuple, label

def jaccard(instance1, instance2):
    j = sum(min(i,j) for (i,j) in zip(instance1, instance2)) / sum(max(x,y) for (x,y) in zip(instance1, instance2))
    return 1 - j

def norm(v):
    return math.sqrt(sum([i**2 for i in v]))

def cosineSimilarity(instance1, instance2):
    dotProd = sum([i * j for (i, j) in zip(instance1, instance2)])
    return 1 - (dotProd / (norm(instance1) * norm(instance2)))

def euclidean(instance1, instance2):
    s = 0
    for i in range(len(instance1)):
        s += (instance1[i] - instance2[i])**2
    return math.sqrt(s)

def manhattan(instance1, instance2):
    s = 0
    for i in range(len(instance1)):
        s += abs(instance1[i] - instance2[i])
    return s

def distance(instance1, instance2, dist_metric):
    if (dist_metric == 'euclidean'):
        return euclidean(instance1, instance2)
    elif (dist_metric == 'cos'):
        return cosineSimilarity(instance1, instance2)
    elif (dist_metric == 'jaccard'):
        return jaccard(instance1, instance2)
    elif (dist_metric == 'manhattan'):
        return manhattan(instance1, instance2)
    return math.inf

def initCentroids(data, k):
    return random.sample(data, k)

def assignClusters(centroids, data, dist_metric):
    # create a dict of cluster to list
    clusters = {}
    for i in range(len(centroids)):
        clusters[i] = []
    # We assign to cluster based on minimum distance, start at infinity
    min = math.inf
    cluster = 0
    # for each row of data
    for datum in data:
        min = math.inf
        # find the closest centroid
        for i in range(len(centroids)):
            dist = distance(datum, centroids[i], dist_metric)
            if dist < min:
                min = dist
                cluster = i
        # group the datum by centroid
        clusters[cluster].append(datum)
    return clusters

def mean(l):
    return sum(l) / len(l)

def nDimMean(l):
    # Assuming each tuple is the same length
    n = len(l[0])
    sums = [0] * n
    for i in range(len(l)):
        for j in range(n):
            sums[j] += l[i][j]
    return [sum / len(l) for sum in sums]

def calculateCentroids(clusters):
    means = []
    # find the mean for each cluster and return an array of the centroids
    for data in clusters.values():
        means.append(tuple(nDimMean(data)))
    return means

def printDict(d):
    print("-- Clusters --")
    for x in d.keys():
        print(x)
        for val in d[x]:
            print(val)

def printCentroids(l):
    print("-- Centroids --")
    for i in range(len(l)):
        print("\t{} : {}".format(i, l[i]))

def kmeans(data, k, dist_metric='euclidean', initial_points=None):
    if initial_points != None:
        centroids = initCentroids(data, k)
    else:
        centroids = initial_points
    # Get the first clusters based on initial centroids
    clusters = assignClusters(centroids, data, dist_metric)
    # prevCentroids to check when to end the loop
    prevCentroids = ()
    iteration = 0
    while (prevCentroids != centroids):
        iteration += 1
        prevCentroids = centroids
        # calculate new centroids
        centroids = calculateCentroids(clusters)
        # calculate new clusters
        clusters = assignClusters(centroids, data, dist_metric)
    print("Iterations: ", iteration)
    return centroids, clusters

def assignLabels(clusters, data, labels):
    clusterToLabels = {}
    clusterToLabel = {}
    for i in clusters.keys():
        clusterToLabels[i] = []
    for i in range(len(data)):
        for j in range(len(clusters.keys())):
            if data[i] in clusters[j]:
                clusterToLabels[j].append(labels[i])
    counts = {}
    for i in clusterToLabels.keys():
        for j in clusterToLabels[i]:
            if j in counts:
                counts[j] += 1
            else:
                counts[j] = 1
        max = 0
        index = 0
        for j in counts.keys():
            if counts[j] > max:
                max = counts[j]
                index = j
        clusterToLabel[i] = index
        cardinality = sum(val for val in counts.values())
        accuracy = counts[index] / cardinality
        print('Accuracy of {} cluster is {}'.format(index, accuracy))
        counts = {}
    print(clusterToLabel)

def sse(centroids, clusters):
    sses = []
    sse = 0
    for i in range(len(clusters)):
        for j in clusters[i]:
            sum = 0
            for x in range(len(j)):
                sum += (centroids[i][x] - j[x])**2
            sse += sum
        sses.append(sse)
        sse = 0
    return sses

####### PART 1 #######
data, labels = loadCSV("part1.csv")

print(" @@@@@@@@@@@@@@@@@ PART 1 @@@@@@@@@@@@@@@@@")
# Q1
start = [(4,6),(5,4)]
centroids, clusters = kmeans(data, 2, dist_metric='manhattan', initial_points=start)
print("Question 1:\n")
print("Centroids:", centroids)
printDict(clusters)
# Q2
start = [(4,6),(5,4)]
centroids, clusters = kmeans(data, 2, dist_metric='euclidean', initial_points=start)
print("Question 2:\n")
print("Centroids:", centroids)
printDict(clusters)
# Q3
start = [(3,3),(8,3)]
centroids, clusters = kmeans(data, 2, dist_metric='manhattan', initial_points=start)
print("Question 3:\n")
print("Centroids:", centroids)
printDict(clusters)
# Q4
start = [(3,2),(4,8)]
centroids, clusters = kmeans(data, 2, dist_metric='manhattan', initial_points=start)
print("Question 4:\n")
print("Centroids:", centroids)
printDict(clusters)

####### PART 2 #######
print(" @@@@@@@@@@@@@@@@@ PART 2 @@@@@@@@@@@@@@@@@")
data, labels = loadCSV("iris.csv")
initialCentroids = initCentroids(data, 3)
print("\n -------- SUM OF SQUARES -------- \n")
centroids, clusters = kmeans(data, 3, dist_metric='euclidean', initial_points=initialCentroids)
assignLabels(clusters, data, labels)
printCentroids(centroids)
print(sse(centroids, clusters))
print("\n -------- COSINE SIMILARITY -------- \n")
centroids, clusters = kmeans(data, 3, dist_metric='cos', initial_points=initialCentroids)
assignLabels(clusters, data, labels)
printCentroids(centroids)
print(sse(centroids, clusters))
print("\n -------- JACCARD -------- \n")
centroids, clusters = kmeans(data, 3, dist_metric='jaccard', initial_points=initialCentroids)
assignLabels(clusters, data, labels)
printCentroids(centroids)
print(sse(centroids, clusters))