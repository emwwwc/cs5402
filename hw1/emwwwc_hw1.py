
# coding: utf-8

# coding: utf-8


# In[2]:


import pandas as pd
import numpy as np


# In[6]:


train_df = pd.read_csv('train.csv')
test_df = pd.read_csv('test.csv')


# In[7]:


train_df


# In[14]:


train_drop_df = train_df.drop(['Survived'], axis=1)
train_drop_df


# In[9]:


train_df.describe()


# In[15]:



combine = train_drop_df.append(test_df, ignore_index=True, sort=True)


# In[17]:


combine


# In[19]:


combine.describe()


# In[36]:


valueCounts = {}
valueCounts['Cabin'] = combine['Cabin'].value_counts().tolist()
valueCounts['Embarked'] = combine['Embarked'].value_counts().tolist()
valueCounts['Name'] = combine['Name'].value_counts().tolist()
valueCounts['Sex'] = combine['Sex'].value_counts().tolist()
valueCounts['Ticket'] = combine['Ticket'].value_counts().tolist()


# In[40]:


cabin_count = sum(valueCounts['Cabin'])
embarked_count = sum(valueCounts['Embarked'])
name_count = sum(valueCounts['Name'])
sex_count = sum(valueCounts['Sex'])
ticket_count = sum(valueCounts['Ticket'])

unique_cabin_count = len(valueCounts['Cabin'])
unique_embarked_count = len(valueCounts['Embarked'])
unique_name_count = len(valueCounts['Name'])
unique_sex_count = len(valueCounts['Sex'])
unique_ticket_count = len(valueCounts['Ticket'])


# In[55]:


print("cabin_count:", cabin_count)
print("embarked_count:", embarked_count)
print("name_count:", name_count)
print("sex_count:", sex_count)
print("ticket_count:", ticket_count)

print("unique_cabin_count:", unique_cabin_count)
print("unique_embarked_count:", unique_embarked_count)
print("unique_name_count:", unique_name_count)
print("unique_sex_count:", unique_sex_count)
print("unique_ticket_count:", unique_ticket_count)

print("\n",combine['Cabin'].value_counts()[:1], "\n")
print(combine['Embarked'].value_counts()[:1], "\n")
print(combine['Name'].value_counts()[:1], "\n")
print(combine['Sex'].value_counts()[:1], "\n")
print(combine['Ticket'].value_counts()[:1], "\n")

###################################################
# In[1]:


import pandas as pd
import numpy as np
train_df = pd.read_csv('train.csv')
test_df = pd.read_csv('test.csv')


# In[4]:


rich_people = train_df.loc[train_df['Pclass'] == 1]


# In[172]:


corr = train_df.loc[train_df['Survived'] == 1]
corr['Pclass'].hist()


# In[29]:


female_survivors = len(train_df.loc[train_df['Sex'] == 'female'].loc[train_df['Survived'] == 1]['Survived'].tolist())
male_survivors = len(train_df.loc[train_df['Sex'] == 'male'].loc[train_df['Survived'] == 1]['Survived'].tolist())
print('male survivors:', male_survivors, "| female survivors:", female_survivors)
print('More female than male?', female_survivors > male_survivors)


# In[51]:


import matplotlib.pyplot as plt
cleanedList = [x for x in train_df.loc[train_df['Survived'] == 1]['Age'].tolist() if str(x) != 'nan']
num_bins = 17
plt.hist(cleanedList, num_bins)
plt.title('Age of Survivors')
plt.show()


# In[63]:


fig, axes = plt.subplots(3, 2, sharex = True, sharey = True)

# First row
survivedPclass1 = [x for x in train_df.loc[train_df['Survived'] == 1].loc[train_df['Pclass'] == 1]['Age'].tolist() if str(x) != 'nan']
deadPclass1 = [x for x in train_df.loc[train_df['Survived'] == 0].loc[train_df['Pclass'] == 1]['Age'].tolist() if str(x) != 'nan']

axes[0,0].hist(deadPclass1, num_bins)
axes[0,0].set_title('Pclass = 1 and Survived = 0')

axes[0,1].hist(survivedPclass1, num_bins)
axes[0,1].set_title('Pclass = 1 and Survived = 1')

# Second row
survivedPclass2 = [x for x in train_df.loc[train_df['Survived'] == 1].loc[train_df['Pclass'] == 2]['Age'].tolist() if str(x) != 'nan']
deadPclass2 = [x for x in train_df.loc[train_df['Survived'] == 0].loc[train_df['Pclass'] == 2]['Age'].tolist() if str(x) != 'nan']

axes[1,0].hist(deadPclass2, num_bins)
axes[1,0].set_title('Pclass = 2 and Survived = 0')

axes[1,1].hist(survivedPclass2, num_bins)
axes[1,1].set_title('Pclass = 2 and Survived = 1')

# Third row
survivedPclass3 = [x for x in train_df.loc[train_df['Survived'] == 1].loc[train_df['Pclass'] == 3]['Age'].tolist() if str(x) != 'nan']
deadPclass3 = [x for x in train_df.loc[train_df['Survived'] == 0].loc[train_df['Pclass'] == 3]['Age'].tolist() if str(x) != 'nan']

axes[2,0].hist(deadPclass3, num_bins)
axes[2,0].set_title('Pclass = 3 and Survived = 0')

axes[2,1].hist(survivedPclass3, num_bins)
axes[2,1].set_title('Pclass = 3 and Survived = 1')


# In[97]:


fig, axes = plt.subplots(3, 2, sharex = True, sharey = True)
# X-Axis for 
sexes = ['female', 'male']

# First Row
dead_S = train_df.loc[train_df['Embarked'] == 'S'].loc[train_df['Survived'] == 0].groupby(['Sex']).mean()['Fare'].tolist()
axes[0, 0].set_ylabel('Fare')
axes[0, 0].set_title('Embarked=S & Survived=0')
axes[0, 0].bar(sexes, dead_S)

survived_S = train_df.loc[train_df['Embarked'] == 'S'].loc[train_df['Survived'] == 1].groupby(['Sex']).mean()['Fare'].tolist()
axes[0, 1].set_title('Embarked=S & Survived=1')
axes[0, 1].bar(sexes, survived_S)

# Second Row
dead_C = train_df.loc[train_df['Embarked'] == 'C'].loc[train_df['Survived'] == 0].groupby(['Sex']).mean()['Fare'].tolist()
axes[1, 0].set_ylabel('Fare')
axes[1, 0].set_title('Embarked=C & Survived=0')
axes[1, 0].bar(sexes, dead_C)

survived_C = train_df.loc[train_df['Embarked'] == 'C'].loc[train_df['Survived'] == 1].groupby(['Sex']).mean()['Fare'].tolist()
axes[1, 1].set_title('Embarked=C & Survived=1')
axes[1, 1].bar(sexes, survived_C)

# Third Row
dead_Q = train_df.loc[train_df['Embarked'] == 'Q'].loc[train_df['Survived'] == 0].groupby(['Sex']).mean()['Fare'].tolist()
axes[2, 0].set_ylabel('Fare')
axes[2, 0].set_title('Embarked=Q & Survived=0')
axes[2, 0].bar(sexes, dead_Q)

survived_Q = train_df.loc[train_df['Embarked'] == 'Q'].loc[train_df['Survived'] == 1].groupby(['Sex']).mean()['Fare'].tolist()
axes[2, 1].set_title('Embarked=Q & Survived=1')
axes[2, 1].bar(sexes, survived_Q)


# In[99]:


train_df


# In[117]:


ticketCounts = train_df['Ticket'].value_counts().tolist()
print(ticketCounts)
duplicateTickets = 0
for x in ticketCounts:
    if x > 1:
        duplicateTickets = duplicateTickets + 1

totalUniqueTickets = len(ticketCounts)

print('Ticket duplicate %: ', (float(duplicateTickets) / totalUniqueTickets) * 100)


# In[128]:


combine = train_df.append(test_df, ignore_index=True, sort=True)
cabins = combine['Cabin']
numNans = cabins.isnull().sum()
numNans

        


# In[138]:


# Q16
genders = []
sexlist = combine['Sex'].tolist()
for i in range(len(sexlist)):
    if (sexlist[i] == 'male'):
        genders.append(0)
    else:
        genders.append(1)
        
combine['Gender'] = pd.Series(genders, combine.index)
combine


# In[147]:


# Q17
import random
stdevRange = (15.4, 44.2)
ageList = combine['Age'].tolist()
ageListBoolean = pd.isna(ageList)
for i in range(len(ageList)):
    if ageListBoolean[i] == True:
        ageList[i] = random.uniform(stdevRange[0], stdevRange[1])
        
combine['Age'] = pd.Series(ageList, combine.index)
combine


# In[151]:



combine.groupby(['Embarked']).count()['Survived']


# In[159]:


#Q18
# From above we can see that S is the most common occurence
embarkList = train_df['Embarked'].tolist()
embarkListBoolean = pd.isna(embarkList)
for i in range(len(embarkList)):
    if embarkListBoolean[i] == True:
        embarkList[i] = 'S'
        
train_df['Embarked'] = pd.Series(embarkList, train_df.index)
train_df


# In[169]:


#Q19
#train_df['Fare'].value_counts() <-- based on this 8.0500 is the most common value
fareList = combine['Fare'].tolist()
fareListBoolean = pd.isna(fareList)
for i in range(len(fareList)):
    if fareListBoolean[i] == True:
        fareList[i] = 8.0500
        
combine['Fare'] = pd.Series(fareList, combine.index)
combine


# In[170]:


#Q20
fareList = []
print(len(combine['Fare'].tolist()))
for fare in combine['Fare'].tolist():
    if  fare <= 7.91:
        fareList.append(0)
    elif fare > 7.91 and fare <= 14.454:
        fareList.append(1)
    elif fare > 14.454 and fare <= 31.0:
        fareList.append(2)
    elif fare > 31:
        fareList.append(3)
        
combine['Fare'] = pd.Series(fareList, combine.index)
combine

